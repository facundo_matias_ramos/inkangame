from typing import List
from django.db import models
from building.models import Building

RESTORE_ENERGY_VILLAGER = 10
DEFAULT_ENERGY_VILLAGER = 100


class Villager:
    energy = models.IntegerField()

    def __init__(self, ):
        self._buildings: List[Building] = []
        self.energy = DEFAULT_ENERGY_VILLAGER

    def get_energy(self) -> int:
        """
            return the energy of villager
        """
        return self.energy

    def _build_building(self, building_cost) -> str:
        if self.energy > building_cost:
            self.energy -= building_cost
            self._buildings.pop(0)
            return 'New building is availble'
        else:
            return 'Error, insufficient energy'

    def build(self) -> str:
        """
           if the villager has a pending construction, he builds it.
        """
        if self._buildings:
            building = self._buildings[0]
            return self._build_building(building.cost)
        else:
            return 'Error, empty building list'

    def add_build(self, building) -> None:
        """
           Add building to list of contruction
        """
        self._buildings.append(building)

    def quantity_building_pendent(self) -> int:
        """
            Returns quantity building pendent
        """
        return len(self._buildings)

    @property
    def buildings(self) -> List:
        """
            Returns a list of buildings to build
        """
        return self._buildings

    def rest(self) -> None:
        """
            Restore energy to builder
        """
        self.energy += RESTORE_ENERGY_VILLAGER


class SpecializedVillager(Villager):

    def __init__(self, ):
        super().__init__()

    def build(self) -> str:
        if self._buildings:
            building = self._buildings[0]
            return self._build_building(building.specialized_cost)
        else:
            return 'Error, empty building list'


class BossVillager(Villager):

    def __init__(self):
        super().__init__()
        self._employees: List[Villager] = []

    def send_build_employee(self, villager) -> str:
        """
            if the villager is an employee, send him to build
        """
        if villager in self._employees:
            return villager.build()
        else:
            return 'Error, is not an employee'

    def send_add_build_employee(self, building, villager) -> str:
        """
            if the villager is an employee, send him to add build to list of construction
        """
        if villager in self._employees:
            villager.add_build(building)
            return 'Employee add building'
        else:
            return 'Error, is not an employee'

    def send_rest_employee(self, villager) -> str:
        """
            if the villager is an employee, send him to rest
        """
        if villager in self._employees:
            villager.rest()
            return 'Employee rest'
        else:
            return 'Error, is not an employee'

    def add_employee(self, villager):
        """
            add villager to list of employee
        """
        self._employees.append(villager)

from django.test import TestCase

from building.models import Building
from villager.models import Villager, BossVillager, SpecializedVillager, DEFAULT_ENERGY_VILLAGER


class Test(TestCase):
    def setUp(self) -> None:
        """
            Create villager, specializedVillager, building, BossVillager
        """
        self.villager = Villager()
        self.villager_specialized = SpecializedVillager()
        self.workshop = Building(20, 20)
        self.quarter = Building(40, 30)
        self.employee = Villager()
        self.villager_boss = BossVillager()
        self.villager_boss.add_employee(self.employee)

    def test_energy_villager(self):
        """
            default energy to create a villager
        """
        self.assertEqual(self.villager.get_energy(), DEFAULT_ENERGY_VILLAGER)

    def test_build_workshop(self):
        """
            build workshop, this building has the same energy consumption for any type of villager
        """
        self.villager.add_build(self.workshop)
        self.villager_boss.add_build(self.workshop)
        self.villager_specialized.add_build(self.workshop)
        self.villager.build()
        self.villager_specialized.build()
        self.villager_boss.build()
        self.assertEqual(self.villager.get_energy(), 80)
        self.assertEqual(self.villager_specialized.get_energy(), 80)
        self.assertEqual(self.villager_boss.get_energy(), 80)

    def test_build_quarter_specialized(self):
        """
           build workshop, this building has different energy consumption for some type of villager
           In this case for the specialized villager
       """
        self.villager.add_build(self.quarter)
        self.villager_boss.add_build(self.quarter)
        self.villager_specialized.add_build(self.quarter)
        self.villager.build()
        self.villager_specialized.build()
        self.villager_boss.build()
        self.assertEqual(self.villager.get_energy(), 60)
        self.assertEqual(self.villager_specialized.get_energy(), 70)
        self.assertEqual(self.villager_boss.get_energy(), 60)

    def test_quantity_build_pending_villager(self):
        """
            Construction order quantity control
        """
        self.villager.add_build(self.quarter)
        self.villager.add_build(self.workshop)
        self.villager.add_build(self.quarter)
        self.assertEqual(self.villager.quantity_building_pendent(), 3)

    def test_build_villager_not_energy(self):
        """
            The villager builds quarter 100 - 40 => energy 60
            The villager builds workshop 60 - 20 => energy 40
            The villager builds workshop 60 - 20 => energy 20
            The villager builds quarter 20 - 40 => energy -20 ERROR
        """
        self.villager.add_build(self.quarter)
        self.villager.add_build(self.workshop)
        self.villager.add_build(self.workshop)
        self.villager.add_build(self.quarter)
        self.villager.build()
        self.villager.build()
        self.villager.build()
        self.assertEqual(self.villager.build(), 'Error, insufficient energy')

    def test_villager_rest(self):
        """
            Control Villager rest
            The villager builds quarter 100 - 40 => energy 60
            The villager rest 60 + 10 => energy 70
        """
        self.villager.add_build(self.quarter)
        self.villager.build()
        self.villager.rest()
        self.assertEqual(self.villager.get_energy(), 70)

    def test_boss_send_build_functions(self):
        """
            add build to employee,  send to build a employee and send to rest to employee
        """
        self.villager_boss.send_add_build_employee(self.workshop, self.employee)
        self.assertEqual(self.employee.quantity_building_pendent(), 1)

        self.villager_boss.send_build_employee(self.employee)
        self.assertEqual(self.employee.get_energy(), 80)
        self.assertEqual(self.employee.quantity_building_pendent(), 0)

        self.villager_boss.send_rest_employee(self.employee)
        self.assertEqual(self.employee.get_energy(), 90)

    def test_boss_not_employee(self):
        """
            functions of the boss, without an employee
        """
        self.assertEqual(self.villager_boss.send_add_build_employee(self.workshop, self.villager),
                         'Error, is not an employee')
        self.assertEqual(self.villager_boss.send_build_employee(self.villager),
                         'Error, is not an employee')
        self.assertEqual(self.villager_boss.send_rest_employee(self.villager),
                         'Error, is not an employee')

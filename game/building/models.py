from django.db import models


class Building:
    cost = models.IntegerField()
    specialized_cost = models.IntegerField()

    def __init__(self, cost: int, specialized_cost: int):
        self.cost = cost
        self.specialized_cost = specialized_cost
